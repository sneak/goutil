package util

import (
	"testing"
	"time"
)

func TestFromUnixMilli(t *testing.T) {
	ms := 1542810446506
	ts := TimeFromUnixMilli(int64(ms))
	if ts.UTC().String() != "2018-11-21 14:27:26.506 +0000 UTC" {
		t.Errorf("Expected time to be '2018-11-21 14:27:26.506 +0000 UTC' got '%s'", ts.UTC().String())
	}
}

func TestFromUnixMicro(t *testing.T) {
	ms := 1542810446506000
	ts := TimeFromUnixMicro(int64(ms))
	if ts.UTC().String() != "2018-11-21 14:27:26.506 +0000 UTC" {
		t.Errorf("Expected time to be '2018-11-21 14:27:26.506 +0000 UTC' got '%s'", ts.UTC().String())
	}
}

func TestFromWebkit(t *testing.T) {
	var wk int64 = 13245202142853170
	ts := TimeFromWebKit(wk)
	expected := "2020-09-21 22:49:02.85317 +0000 UTC"
	if ts.UTC().String() != expected {
		t.Errorf("Expected time to be '%s' got '%s'", expected, ts.UTC().String())
	}
}

func TestNowUnixMicro(t *testing.T) {
	now := time.Now()
	nownano := now.UnixNano()
	nowmicro := nownano / 1000
	ts := TimeFromUnixMicro(nowmicro)
	if ts.UTC().String() != now.UTC().String() {
		t.Errorf("Expected '%s' got '%s'", now.UTC().String(), ts.UTC().String())
	}
}

func TestNowUnixMilli(t *testing.T) {
	now := time.Now()
	nownano := now.UnixNano()
	nowmilli := nownano / 1000000
	ts := TimeFromUnixMilli(nowmilli)
	if ts.UTC().Format(time.StampMilli) != now.UTC().Format(time.StampMilli) {
		t.Errorf("Expected '%s' got '%s'", now.UTC().String(), ts.UTC().String())
	}
}
