module sneak.berlin/go/util

go 1.14

require (
	github.com/asaskevich/govalidator v0.0.0-20230301143203-a9d515a09cc2 // indirect
	github.com/hako/durafmt v0.0.0-20191009132224-3f39dc1ed9f4
)
